<?php

namespace App\Http\Controllers;

use App\Models\Pembelian;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LaporanController extends Controller
{
    public function get() 
    {
        $pembelian = Pembelian::select("pembelian.id_barang", DB::raw('SUM((pembelian.harga_jual * pembelian.total_barang) - (pembelian.harga_beli * pembelian.total_barang)) AS keuntungan'), DB::raw("SUM(pembelian.total_barang) AS total_barang"), "barang.nama AS nama_barang")
                            ->join('barang', 'pembelian.id_barang', '=', 'barang.id')
                            ->where('pembelian.is_validate', true)
                            ->groupByRaw('pembelian.id_barang')
                            ->get();
        return response()->json($pembelian, 200);
    }
}
