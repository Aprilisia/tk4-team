<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Pembelian;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PembelianController extends Controller
{
    // public function get() 
    // {
    //     $pembelian = Pembelian::select("pembelian.id", "pembelian.id_barang", "pembelian.total_barang", "barang.nama AS nama_barang", "pembelian.is_validate")
    //                         ->join('barang', 'pembelian.id_barang', '=', 'barang.id')
    //                         ->get();
    //     return response()->json($pembelian, 200);
    // }

    public function getById($id){
        $data = Pembelian::find($id);
        return response()->json($data, 200);
    }

    public function save(Request $request, $id = null) 
    {
        $dataExist = Pembelian::find($id);
        $dataBarang = Barang::find($request['barang']);

        $data['id_barang'] = $request['barang'];
        $data['total_barang'] = $request['total'];
        $data['harga_beli'] = $dataBarang->harga_beli;
        $data['harga_jual'] = $dataBarang->harga_jual;
        $data['is_validate'] = $request['is_validasi'] ?? 0;
        $data['created_by'] = $request['created_by'];
        
        $res = false;
        if(!empty($id)) {
            $res = $dataExist->update($id, $data);
        } else {
            $res = Pembelian::create($data);
        }

        if($res) {
            if($request->role == "Staff") {
                $dataBarang->stok = $dataBarang->stok - 1;
                $dataBarang->save();
                if($dataBarang) {
                    return response()->json([
                        'message' => "Success",
                        'success' => true
                    ], 200);
                }
            } else {
                return response()->json([
                    'message' => "Success",
                    'success' => true
                ], 200);
            }
        }

        return response()->json([
            'message' => "Failed",
            'success' => true
        ], 500);
    }

    public function validasi($id) 
    {
        $dataExist = Pembelian::find($id);

        $data['is_validate'] = 1;
        
        $res = $dataExist->update($data);

        if($res) {
            // if($request->role == "Staff") {
                $dataBarang = Barang::find($dataExist['id_barang']);
                $dataBarang->stok = $dataBarang->stok - $dataExist["total"];
                $dataBarang->save();
                if($dataBarang) {
                    return response()->json([
                        'message' => "Success",
                        'success' => true,
                        'status' => 200
                    ], 200);
                }
            // } else {
            //     return response()->json([
            //         'message' => "Success",
            //         'success' => true
            //     ], 200);
            // }
        }

        return response()->json([
            'message' => "Failed",
            'success' => true
        ], 500);
    }

    public function delete($id){
        $res = Pembelian::find($id)->delete();
        if($res) {
            return response()->json([
                'message' => "Successfully deleted",
                'success' => true
            ], 200);
        }
        
        return response()->json([
            'message' => "Failed deleted",
            'success' => true
        ], 500);
    }
}
