import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PembelianRoutingModule } from './pembelian-routing.module';
import { IndexComponent } from './index/index.component';
import { FormComponent } from './form/form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    IndexComponent,
    FormComponent
  ],
  imports: [
    CommonModule,
    PembelianRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PembelianModule { }
