import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})

export class FormComponent implements OnInit {
  id_user = localStorage.getItem("id_user");
  username = localStorage.getItem("username");
  role = localStorage.getItem("role");
  isLoggedIn = localStorage.getItem("username") && localStorage.getItem("role") ? true : false;

  id: number | any;
  barang: any;
  form: FormGroup | any;
  filedata: any;
  gambar: any

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    if(this.isLoggedIn && this.role != "Pembeli") {
      this.id = this.route.snapshot.params['idBarang'] ?? "";
      
      this.form = this.formBuilder.group({
        nama:  new FormControl("", [ Validators.required ]),
        deskripsi:  new FormControl("", [ Validators.required ]),
        jenis:  new FormControl('', [ Validators.required ]),
        stok:  new FormControl('', [ Validators.required ]),
        harga_beli:  new FormControl('', [ Validators.required ]),
        harga_jual:  new FormControl('', [ Validators.required ]),
        gambar:  new FormControl(''),
        created_by:  new FormControl(this.id_user, [ Validators.required ]),
      });
  
      if(this.id) {
        this.http.get("http://127.0.0.1:8000/api/barang/" + this.id)
        .subscribe(x => this.form.patchValue(x))
      }
    } else {
      if(this.role == "Pembeli")
        this.router.navigateByUrl('/')
      else
        this.router.navigateByUrl('/login')
    }
  }

  get f(){
    return this.form.controls;
  }

  onFileChange(e:any){
    this.form.patchValue({
      gambar: e.target.files[0]
    })
  }

  submit(){
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json'
      })
    };
    var formData = new FormData();
    formData.append('nama', this.form.value.nama);
    formData.append('deskripsi', this.form.value.deskripsi);
    formData.append('jenis', this.form.value.jenis);
    formData.append('stok', this.form.value.stok);
    formData.append('harga_beli', this.form.value.harga_beli);
    formData.append('harga_jual', this.form.value.harga_jual);
    formData.append('gambar', this.form.controls['gambar'].value);
    formData.append('created_by', this.form.value.created_by);
    
    this.http.post("http://127.0.0.1:8000/api/barang/" + this.id ?? "", formData, httpOptions)
      .subscribe(data => {
        alert("Success")
        this.router.navigateByUrl('barang/index');
      }); 
  }
}
