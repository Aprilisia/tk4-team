import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  username = localStorage.getItem("username");
  role = localStorage.getItem("role");
  isLoggedIn = localStorage.getItem("username") && localStorage.getItem("role") ? true : false;
  user: any;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    if(this.isLoggedIn && this.role != "Pembeli") {
      this.http.get("http://127.0.0.1:8000/api/user/")
      .subscribe(data => {
        this.user = data;
      })
    } else {
      if(this.role == "Pembeli")
        this.router.navigateByUrl('/')
      else
        this.router.navigateByUrl('/login')
    }
  }

  deleteUser(id: number){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': "application/json"
      })
    };

    this.http.delete("http://127.0.0.1:8000/api/user/" + id, httpOptions)
    .subscribe(data => {
      this.user = this.user.filter((item: { id: number; }) => item.id !== id);
      alert("Success")
    })
  }
}
