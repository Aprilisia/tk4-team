import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Chart,PieController, ArcElement, PointElement, LinearScale, Title, Filler, Colors, CategoryScale, Tooltip,Legend } from 'chart.js';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  username = localStorage.getItem("username");
  role = localStorage.getItem("role");
  isLoggedIn = localStorage.getItem("username") && localStorage.getItem("role") ? true : false;
  barang: any;

  constructor(private http: HttpClient, private router: Router) { 
    Chart.register(PieController, ArcElement, PointElement, LinearScale, Title, Filler, Colors, CategoryScale, Tooltip, Legend);
  }

  ngOnInit(): void {
    if(this.isLoggedIn && this.role != "Pembeli") {
      let dataLabel: any[] = [];
      let dataTotalItem: number[] = [];
      let dataKeuntungan: number[] = [];
      fetch(`http://127.0.0.1:8000/api/laporan`)
        .then((response) => {
          return response.json();
        })
        .then((data) => {
            data.map((result: any) => {
              dataLabel.push(result.nama_barang)
              dataTotalItem.push(result.total_barang)
              dataKeuntungan.push(parseInt(result.keuntungan))
            });
            new Chart("myChart", {
              type: 'pie',
              data: {
                labels: dataLabel,
                datasets: [
                  {
                    label: "Total Penjualan Barang",
                    data: dataTotalItem,
                    borderWidth: 1
                  },
                  {
                    label: "Total Keuntungan Penjualan",
                    data: dataKeuntungan,
                    borderWidth: 1
                  },
                ]
              },
              options: {
                scales: {
                  y: {
                    beginAtZero: true
                  }
                }
              }
            });
        })
        .catch((err) => {
          alert(err.message);
          console.log(err);
        });
    } else {
      if(this.role == "Pembeli")
        this.router.navigateByUrl('/')
      else
        this.router.navigateByUrl('/login')
    }
  }
}
